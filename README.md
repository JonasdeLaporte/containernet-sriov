
# Containernet-SRIOV

Containernet-SRIOV is a fork of the network emulator
[Containernet](https://github.com/containernet/containernet). The goal of the project is to add hardware support for
physical NICs to Containernet hosts with
[SRIOV](https://www.intel.com/content/www/us/en/content-details/321211/pci-sig-sr-iov-primer-an-introduction-to-sr-iov-technology.html),
to allow to build emulated testbeds that perform closer to
a fully hardware-based testing, both in performance as well
as in resiliance to influences like high CPU or RAM
utilization. This project was part of my Bachelor Thesis.
The README contains excerpts from my thesis.

Besides SRIOV, this extension makes it possible to directly
assign physical NICs to the Containernet hosts.

## Table of Contents

- [SRIOV](#sriov)
- [Prepearing the environment](#prepearing-the-environment)
- [Creating a topology](#creating-a-topology)
- [Starting Containernet with SRIOV](#starting-containernet-with-sriov)


## SRIOV
![img: SRIOV functions with containernet visualized](/assets/sriov.png)

Single root input/output virtualization is a technology introduced by PCI-SIG that
allows one single I/O device to appear as multiple, different devices. For that,
two functions are introduced: The physical function (PF) and virtual function (VF).
The PF can be configured and used like a distinct physical PCIe device, while the
VFs are lightweight functions. This means that VF do not require separate resource
configuration but inherit their functionality from the PF. They are developed to
resemble non-virtualized devices, which provides the possibility that existing drivers
may be compatible with the VFs which, in turn, saves driver development costs. VFs
have their own base address registers (BARs) and configuration space, which makes
it possible to directly assign a virtual function to a VM by mapping the base address
register spaces into VM memory. This bypasses the virtual machine monitor and the
VM can perform I/O operations directly.

The different VFs will be assigned to the different Containernet
hosts, which use them as if they are distinct NICs.

Traffic traversing the VFs will be handled by the same NIC and therefore be sent over
the same physical connection. To distinguish the frames on the physical medium, an
abstraction called virtual LAN (VLAN) is used.

![img: SRIOV connection visualized](/assets/sriov_connection.png)

## Prepearing the environment

Before SRIOV can be used the correct driver is to be installed
and supporting network interface cards need to be configured.
``` bash
# despawn any existing virtual functions
echo 0 > /sys/class/net/<PF>/device/sriov_numvfs

# spawn virtual functions
echo <NR> > /sys/class/net/<PF>/device/sriov_numvfs
```
The placeholder <PF> denotes the identifier of the physical
and <NR> the amount of virtual functions to create.

The following commands need to be executed for every
virtual function created in the previous step:
```bash
# trust virtual function
ip link set dev <PF> vf <VF ID> trust on

# disable spoof check
ip link set <PF> vf <VF ID> spoofchk off

# set MAC address
ip link set <PF> vf <VF ID> mac <MAC>

# enable VLAN stripping
echo on > /sys/class/net/<PF>/device/sriov/<VFID>/vlan_strip
```
In order to be able to use the promiscuous mode we need to set the corresponding
virtual function to be trusted. Otherwise, requests for promiscuous mode will be ignored
by the physical function. We further disable the Intel malicious driver detection
(MDD) feature, since this can have a performance impact and 
may otherwise cause unforseen influences. Enabling
VLAN  stripping will instruct the VLAN tag to be removed from the
Ethernet frame as soon as the frame leaves the VLAN. This
is done to make the internal implementation transparent to
an outside observer.

Finally, we need to configure true promiscuous mode. By
default, the promiscuous mode is limited when only allowed
on the virtual functions.
We further configure the use of unicast promiscuous mode. This is required as the
physical NIC will receive frames that are not destined to its addresses.
```bash
# enable unicast promiscous mode on PF
ip link set <PF> promisc on

# unlimit promiscous mode
ethtool --set-priv-flags <PF> vf-true-promisc-support on
```

## Creating a topology
The following is an example of how to create a topology and
how to deploy it with Containernet. This can be further
aopted to suit the needed use case. We define a new
topology by inheriting from the ``Topo`` class and
overwriting the the ``build()`` function.
```python
from mininet.topo import Topo
from mininet.node import Docker

# custom topology
class ChainTopo(Topo):

def build(self, number_nodes, physical_functions, **_opts):
  pf1 = physical_funtions[0],
  pf2 = physical_functions[1],
  hosts=[]

  # adding hosts
  for i in range(1, number_nodes + 1):
    h = self.addHost('h%d' % i, cls=Docker, dimage=" ubuntu:trusty")
    hosts.append(h)

  # adding links
  for i in range (0 , number_nodes - 1):
    h1 = hosts[i]
    h2 = hosts[i+1]

    # derive VF name following format <PF>v<ID>
    if1 = '%sv%d' % (pf2, i)
    if2 = '%sv%d' % (pf1, i+1)

    self.addLink(h1, h2, intfName1=if1, intfName2=if2)
```
This creates a simple chain of hosts. In this case, we
define the available physical functions in a list called
``physical_functions`` and hand this list to
the ``build()`` function of our topology class. We do this
because the i40e driver for the X710 network interface
cards - which we used for testing - creates the virtual
functions in the format ``<PFname>v<VFnumber>``, starting
with VFID zero.

## Starting Containernet with SRIOV

The topology can then be used like
```python
from mininet.net import Containernet
from mininet.cli import CLI

topo = ChainTopo(numberHosts, physical_functions)
net = Containernet (topo=topo, sriov=True)

net.start()
CLI(net)
net.stop()
```
To enable SRIOV support, the ``sriov`` parameter is given as parameter when invoking the constructor of Containernet.
