#!/bin/bash

# exit on error
set -e
# log every command
set -x

# Debian bullseye seems to have some problems with docker. Following patches need to be applied:
# 1) set environment variable DOCKER_RAMDISK=true https://stackoverflow.com/questions/57418495/docker-on-machine-using-a-ramdisk-doesnt-work
# 2) change cgroup driver in systemd unit file https://hadoop.apache.org/docs/r3.3.0/hadoop-yarn/hadoop-yarn-site/DockerContainers.html
#    unit file: /usr/lib/systemd/system/docker.service
# 3) optional? https://docs.docker.com/engine/install/debian/

echo "Fixing docker installation on debian"
echo ""

# fix 1)
echo "-> setting DOCKER_RAMDISK environment variable"
mkdir -p /etc/systemd/system/docker.service.d
sleep 2
cp "$HOME"/containernet/bin/prepare/10-ramdisk.conf /etc/systemd/system/docker.service.d/
echo "done"

# fix 2)
echo "-> changing cgoup driver to cgroupfs"
rm /usr/lib/systemd/system/docker.service
sleep 2
cp "$HOME"/containernet/bin/prepare/docker.service /usr/lib/systemd/system/
echo "done"

echo "-> restarting docker"
systemctl daemon-reload
systemctl restart docker
echo "all done!"