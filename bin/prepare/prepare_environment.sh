#!/bin/bash
# This script is intended to prepare the UNIX-like (tested for Debian) environment
# for the the use of the containernet prototype which uses SRIOV instead of
# virtual ethernet pairs (VETH). To do so, the following dependencies will be
# installed:
# - i40e-2.18.9 Intel driver for support of X710er Intel NICs
# - ethtool
# -
# furthermore it will enable SRIOV virtual functions

# Some hardware configurations support fewer SR-IOV instances, as the whole Intel
# Ethernet Controller XL710 (all functions) is limited to 128 SR-IOV interfaces
# in total. [1]


# for the Intel X710er NIC, this value has to be in the intevall [1,32]

if [ $1 == "--help" ]; then
  echo "Usage ./prepare_environment.sh <deps> <num_vfs> <pf1> <pf2>"
  exit
fi

NUMVFS=11
RAND_MAC="00:00:00:00:00:00"

random_mac () {
    # stole this from https://serverfault.com/questions/299556/how-to-generate-a-random-mac-address-from-the-linux-command-line
    RAND_MAC=$(echo -n 02; od -t x1 -An -N 5 /dev/urandom | tr ' ' ':')
}

echo ""
echo "========================================================================="
echo "                     Preparing the environment                           "
echo "========================================================================="
echo ""

# specify the SRIOV capable network interface here TODO BA automate that
# === mtgox ===
#IFACE1="ens3f0"
#IFACE2="ens6f0"
# === amdexp1 ===
IFACE1="enp33s0f0"
IFACE2="enp100s0f0"

if [ $# -eq 4 ]; then
    NUMVFS=$2
    IFACE1=$3
    IFACE2=$4
fi

echo "-> number of VFs is set to $NUMVFS!"

echo "->using PFs '$IFACE1' and '$IFACE2'"

# path to the file that contains the number of virtual functions
SRIOVNUMVFS1="/sys/class/net/$IFACE1/device/sriov_numvfs"
SRIOVNUMVFS2="/sys/class/net/$IFACE2/device/sriov_numvfs"

# install ethtool. The driver utilizes the ethtool interface for driver configuration and
# diagnostics, as well as displaying statistical information [1]

if [[ $1 == "deps" ]]
then
  echo ""
	echo "###########################"
	echo "# Installing dependencies #"
	echo "###########################"
	echo ""
	apt-get -y install ethtool

	cd $HOME

	echo ""
	echo "-> installing newest intel NIC driver i40e"
	echo ""
	# download, extract and install the i40e Intel driver TODO BA check for new version
	wget "https://downloadmirror.intel.com/727503/i40e-2.18.9.tar.gz"
	tar zxf i40e-2.18.9.tar.gz
	cd i40e-2.18.9/src/
	make install

  depmod -a
  MODULE="i40e"

	echo ""
	echo "-> loading driver"
	echo ""
  if lsmod | grep "$MODULE" &> /dev/null ; then
    rmmod i40e; modprobe i40e
  else
    modprobe i40e
  fi

	cd $HOME
	rm -r i40e-2.18.9*

	echo ""
  echo "#######################"
  echo "# Fixing docker setup #"
  echo "#######################"
  echo ""

  "$HOME"/containernet/bin/prepare/fix_docker_debian.sh
  echo ""
  echo "loading pre-build docker image ubuntu:trusty"
  docker load -i "$HOME"/containernet/bin/prepare/ubuntu_trusty.tar
  #cd "$HOME"/containernet/examples/example-containers/
  #./build.sh
else
	echo ""
	echo "Skipping installation of dependencies"
	echo ""
fi

echo "################################"
echo "# Setting up virtual functions #"
echo "################################"
echo ""

echo "-> setting dmesg log level to 8"
dmesg -n 8

# reset VFs in case there were any created before
echo 0 > $SRIOVNUMVFS1
echo 0 > $SRIOVNUMVFS2

sleep 5

# disable VLAN pruning
echo "disabling VLAN pruning"
ethtool --set-priv-flags $IFACE1 vf-vlan-pruning off
ethtool --set-priv-flags $IFACE2 vf-vlan-pruning off

# enable SRIOV by defining the amount of virtual functions that shall be created
echo "creating virtual functions..."
echo "-> $NUMVFS VFs on $IFACE1"
echo $NUMVFS > $SRIOVNUMVFS1
echo "-> $NUMVFS VFs on $IFACE2"
echo $NUMVFS > $SRIOVNUMVFS2

sleep 5

echo ""
echo "configuring virtual functions..."
for (( i=0 ; i<$NUMVFS; i++ )); 
do
	# if VFs are not trusted, promiscous mode requests will be ignored by the PF [1]
	echo "-> trusting VF$i of PF $IFACE1"
	ip link set dev $IFACE1 vf $i trust on
	echo "-> trusting VF$i of PF $IFACE2"
	ip link set dev $IFACE2 vf $i trust on
	
	# disable spoofcheck detection
	echo "-> disabeling spoof check on VF$i of PF $IFACE1"
	ip link set $IFACE1 vf $i spoofchk off
	echo "-> disabeling spoof check on VF$i of PF $IFACE2"
	ip link set $IFACE2 vf $i spoofchk off

  random_mac
	ip link set $IFACE1 vf $i mac $RAND_MAC
	echo "-> setting MAC address of VF$i (PF $IFACE1) to $RAND_MAC"
	random_mac
	ip link set $IFACE2 vf $i mac $RAND_MAC
	echo "-> setting MAC address of VF$i (PF $IFACE2) to $RAND_MAC"

	echo "-> enable VLAN stripping for VF$i (PF $IFACE1) (only required for latency tests with MoonGen/Libmoon)"
	echo on > /sys/class/net/$IFACE1/device/sriov/$i/vlan_strip
	echo "-> enable VLAN stripping for VF$i (PF $IFACE2) (only required for latency tests with MoonGen/Libmoon)"
	echo on > /sys/class/net/$IFACE2/device/sriov/$i/vlan_strip

	ip link set $IFACE1 vf $i state enable
	ip link set $IFACE2 vf $i state enable

	echo ""
done

# enable unicast promiscous mode
echo "-> enabeling unicast promiscous mode on PF $IFACE1"
ip link set $IFACE1 promisc on
echo "-> enabeling unicast promiscous mode on PF $IFACE2"
ip link set $IFACE2 promisc on

# by default, ethtool limits the promiscous mode of the VFs, so we are unlimiting it here.
# this does NOT enable promiscous mode. It only defines which promiscous mode will be used
# when it is enabled via 'ip link' commands [1]
echo "-> unlimit promiscous mode on PF $IFACE1"
ethtool --set-priv-flags $IFACE1 vf-true-promisc-support on
echo "-> unlimit promiscous mode on PF $IFACE2"
ethtool --set-priv-flags $IFACE2 vf-true-promisc-support on

ip link set dev $IFACE1 up
ip link set dev $IFACE2 up

# Sources:
# 1. Intel i40e driver README, available at https://downloadmirror.intel.com/727503/readme.txt
