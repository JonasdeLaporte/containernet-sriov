#!/bin/bash

DIR=/root

# update apt mirror list
apt-get update --fix-missing 

# install necessary dependencies for ansible auto install
apt-get -y install ethtool ansible sudo

# ignore last error about docker installation for experiment failing since it is not
# relevant for the BA tests
sudo ansible-playbook -i "localhost," -c local $DIR/containernet/ansible/install_debian.yml


