#!/usr/bin/env python
"""
This creates l2 network chain of hosts with two interfaces each. One interface of each the
first and the last host will be left unused to serve as ingress/egress points for
e.g. MoonGen.
"""
import os
import random
from optparse import OptionParser

from mininet.net import Mininet
from mininet.topo import Topo
from mininet.node import Node
from mininet.log import setLogLevel, info
from mininet.cli import CLI
from mininet.link import Intf, TCLink


class Router(Node):
    # pylint: disable=arguments-differ
    def config( self, **params ):
        super(Router, self).config(**params)
        # Enable forwarding on the router
        self.cmd( 'sysctl net.ipv4.ip_forward=1' )

        default_route=None
        #default_route = self.params.get("default_route").split("/")[0]
        default_intf = self.params.get("default_intf")
        #TODO BA manually preventing the default route from being set to test the performance of a linux bridge
        if default_route:
            # TODO BA default route interface backup rewrite as this is basically picking randomly
            if not default_intf: default_intf = self.intfs.get(list(self.intfs)[-1]).name
            self.cmd("ip route del default")
            self.cmd("ip route add default via %s dev %s" % (default_route, default_intf))

    def terminate( self ):
        #info("chainTopo.terminate(): disabling forwarding\n")
        self.cmd( 'sysctl net.ipv4.ip_forward=0' )
        super(Router, self).terminate()


class ChainTopo(Topo):
    # pylint: disable=arguments-differ
    def build( self, n, pfs, ingress, egress, **_opts ):
        pf1 = pfs[0]
        pf2 = pfs[1]
        #info("chainTopo.ChainTopo.build(): pf1 = %s, pf2 = %s\n" % (pf1, pf2))
        hosts = []

        # adding hosts
        for i in range( 1, n+1 ):
            h = self.addHost( 'h%d' % i, cls=Router )
            hosts.append(h)

        # adding links
        for i in range( 0, n-1 ):
            h1 = hosts[i]
            h2 = hosts[i+1]

            #TODO BA maybe include management of virtual functions into net.py
            #info("-> adding link %s-%s\n" % (h1, h2))

            #TODO BA add ingress/egress here

            if1='%sv%d' % (pf2, i)
            if2='%sv%d' % (pf1, i+1)

            #=====

            prefixLen = 24

            ip1 = '10.0.%d.1/%d' % (i + 1, prefixLen)
            ip2 = '10.0.%d.2/%d' % (i + 1, prefixLen)

            params1 = {}
            params2 = {}

            params1.setdefault('ip', ip1)
            params1.setdefault('prefixLen', prefixLen)
            params2.setdefault('ip', ip2)
            params2.setdefault('prefixLen', prefixLen)

            #=====

            self.addLink(h1, h2, intfName1=if1, intfName2=if2, params1=params1, params2=params2)

    '''
    def addLink(self, h1, h2, **opts):
        #TODO BA create VLANs here for now (integrate it naturally into Topo by creating new subclass)
        #info("chainTopo.ChainTopo.addLink(): called overwritten function\n")
        Topo.addLink(self, h1, h2, **opts)
    '''


class VirtualChainTopo( Topo ):


    # pylint: disable=arguments-differ
    def build( self, n, **_opts ):
        info("***Starting original mininet chain with %s nodes\n" % n)
        hosts = []
        for i in range( 1, n+1 ):
            info("-> creating host %s\n" % str(i))
            h = self.addHost( 'h'+str(i), cls=Router )
            hosts.append(h)
        info("Hosts: %s\n" % hosts)
        for i in range( 0, n-1 ):
            h1 = hosts[i]
            h2 = hosts[i+1]
            info("-> adding link %s-%s\n" % (h1, h2))
            # use TCLink class; limiting bandwidth to 1000 Mbit/s (physical connections are litmited like this)
            self.addLink(h1, h2, bw=1000)


def getIPs(host_number):
    #info("creating IPs for host %d:\n" % host_number)
    if host_number == 1:
        ip1_net = "10.0.0."
    else:
        ip1_net = "10.0." + str(host_number - 1) + "."

    ip1_net = "10.0.%d." % (host_number - 1)
    ip2_net = "10.0.%d." % host_number
    prefix_len = "/24"

    ip1 = ip1_net + "2" + prefix_len
    ip2 = ip2_net + "1" + prefix_len
    #info("->ingress IP: %s, egress IP: %s\n" % (ip1, ip2))

    return ip1, ip2


def random_mac():
    # stolen from https://stackoverflow.com/questions/50187634/random-mac-address-generator-in-python
    myhexdigits = []
    for x in range(6):
        # x will be set to the values 0 to 5
        a = random.randint(0, 255)
        # a will be some 8-bit quantity
        hex = '%02x' % a
        # hex will be 2 hexadecimal digits with a leading 0 if necessary
        # you need 2 hexadecimal digits to represent 8 bits
        myhexdigits.append(hex)
        # save for after the loop ends
    mac = ':'.join(myhexdigits)
    return mac
    # using : as the delimiter, join the 2-digit hex strings together into
    # a single string


def setVLAN(vf_intf_name, vf_id):
    split_name = vf_intf_name.split("v")
    pf_name = split_name[0]
    vf_id = split_name[1]

    os.system("ip link set %s vf %s vlan %s" % (pf_name, vf_id, 0))
    os.system("ip link set %s vf %s vlan %s" % (pf_name, vf_id, vf_id))


def sriov_topo_net(num_hosts, physical_functions, ingress, egress):
    topo = ChainTopo(num_hosts, physical_functions, ingress, egress)
    net = Mininet(topo=topo, sriov=True)#, physical_functions=physical_functions)
    dest_ip = '10.1.0.10'

    ingr_iface = Intf(ingress, node=net['h1'])
    egr_iface = Intf(egress, node= net['h%d' % num_hosts])

    # configure IPs

    for k in range(1, num_hosts+1):
        h = net['h%d' % k]
        prefixlen = '24'

        if k == 1:
            ingress = h.intfs[1]
            egress = h.intfs[0]
        else:
            ingress = h.intfs[0]
            egress = h.intfs[1]

        ipstr_ingress = '10.0.%d.2' % k
        #ingress.setIP(ipstr_ingress, prefixLen=prefixlen, overwrite=True)

        if k == num_hosts:
            ipstr_egress = '10.1.0.1'
            prefixlen = '16'
            default_ip = dest_ip
        else:
            ipstr_egress = '10.0.%d.1' % (k + 1)
            default_ip = '10.0.%d.2' % k

        #egress.setIP(ipstr_egress, prefixLen=prefixlen, overwrite=True)
        #h.cmd("ip route add default via %s dev %s" % (default_ip, egress))

        #info("\nchainTopo.sriov_topo_net(): h%d setting default route\n" % k)
        #h.setDefaultRoute(intf=("via %s dev %s" % (default_ip, egress)))
        
        #TODO BA samples are not getting collected

    ingr_iface.setIP('10.0.0.2', prefixLen='16', overwrite=True)
    egr_iface.setIP('10.1.0.1', prefixLen='16', overwrite=True)


    # setup bridges
    for i in range(1, num_hosts+1):
        h = net['h%d' % i]
        _intfs = h.intfs
        _iface1 = _intfs.get(0)
        _iface2 = _intfs.get(1)

        setup_bridge(h, _iface1, _iface2)
    return net


def setup_bridge( node, iface1, iface2 ):
    cmds = []
    #os.system("ip link add br0 type bridge")
    #Intf('br0', node=node, moveIntfFn=None)

    #info("\nsetting up bridge between %s and %s\n" % (iface1, iface2))

    cmds.append("ip link add br0 type bridge")
    cmds.append("ip link set %s master br0" % iface1)
    cmds.append("ip link set %s master br0" % iface2)
    cmds.append("ip link set br0 up")

    for c in cmds:
        node.cmd(c)

    #mac = random_mac()
    #info("Generated MAC %s\n" % mac)
    #node.setMAC(mac, intf='br0')


def attach_moongen(first_node, last_node, net, ingress_iface, egress_iface):
    # add physical ingress/egress interfaces
    h1, hn = net[str(first_node)], net[str(last_node)]
    info("-> adding physical ingress/egress to %s and %s\n" % (h1, hn))
    Intf(ingress_iface, node=h1)
    Intf(egress_iface, node=hn)


def configure_moongen(last_node, net, ingress_iface, egress_iface):
    hn = net[str(last_node)]
    hn.setIP(ip='10.1.0.1', prefixLen=16, intf=egress_iface, overwrite=True)
    hn.setDefaultRoute(intf=egress_iface)
    setup_bridge(hn, ingress_iface, egress_iface)


def configure_chain(net, n, ingress, egress):
    DSTIP = '10.1.0.10'

    for i in range(1,n):

        h1_ingress_iface = 'h%d-eth0' % i
        h1_egress_iface = 'h%d-eth1' % i
        #h1_egress_ip = '10.0.%d.1' % i
        #h2_ingress_iface = 'h%d-eth0' % (i+1)
        #h2_ingress_ip = '10.0.%d.2' % i

        if i == 1:
            h1_ingress_iface = ingress
            h1_egress_iface = 'h%d-eth0' % i
        elif i == n:
            h1_egress_iface = egress

        h1 = net['h%d' % i]
        #h2 = net['h%d' % (i+1)]

        #h1.setIP(ip=h1_egress_ip, prefixLen=24, intf=h1_egress_iface, overwrite=True)
        #h2.setIP(ip=h2_ingress_ip, prefixLen=24, intf=h2_ingress_iface, overwrite=True)

        setup_bridge(h1, h1_ingress_iface, h1_egress_iface)
        #h1.setDefaultRoute(intf=('via %s dev %s' % (h2_ingress_ip, h1_egress_iface)))
        #h1.cmd( 'ip route a %s via %s dev %s' % (DSTIP, h2_ingress_ip, h1_egress_iface))


def original_net(n, ingress, egress):
    if not n:
        n = 3

    topo = VirtualChainTopo(n)
    net = Mininet(topo=topo, link=TCLink)

    attach_moongen('h1', 'h%d' % n, net, ingress, egress)
    '''
    for i in range(1, n):
        h1 = net['h%d' % i]
        h2 = net['h%d' % (i+1)]
        if i == 1:
            h1_ingress_iface = ''
            h1_egress_iface = 'h1-eth0'
            os.system("pos_set_variable containernet/first_host_pid %s" % h1.pid)
        else:
            h1_ingress_iface = 'h%d-eth0' % i
            h1_egress_iface = 'h%d-eth1' % i
        h1_egress_ip = '10.0.%d.1' % i
        h2_ingress_iface = 'h%d-eth0' % (i+1)
        h2_ingress_ip = '10.0.%d.2' % i

        h1.setIP(ip=h1_egress_ip, prefixLen=24, intf=h1_egress_iface, overwrite=True)
        h2.setIP(ip=h2_ingress_ip, prefixLen=24, intf=h2_ingress_iface, overwrite=True)

        #setup_bridge(h1, h1_ingress_iface, h1_egress_iface)
        h1.setDefaultRoute(intf=('via %s dev %s' % (h2_ingress_ip, h1_egress_iface)))
        h1.cmd( 'ip route a %s via %s dev %s' % (DSTIP, h2_ingress_ip, h1_egress_iface))

        if i == (n-1):
            info("-> on host h%d rn but configuring last host h%d\n" % (i, (i+1)))
            os.system("pos_set_variable containernet/last_host_pid %s" % h2.pid)
            #h2.setDefaultRoute(intf=('via %s dev %s' % (DSTIP, EGRESS)))
            #h2.cmd('ip route a %s via %s dev %s' % (DSTIP, DSTIP, EGRESS))
            h2.setDefaultRoute(intf=egress)
    '''
    configure_chain(net, n, ingress, egress)
    configure_moongen('h%d' % n, net, 'h%d-eth0' % n, egress)
    return net


def run(num_hosts, ifaces, org, ingress, egress):
    #TODO BA edge cases for ingress, egress = None, None
    if org:
        net = original_net(num_hosts, ingress, egress)
    else:
        net = sriov_topo_net(num_hosts, ifaces, ingress, egress)
    net.start()
    while True: pass
    #CLI( net )
    #net.stop()


def verbose( _option, _opt_str, value, _parser ):
    if not value:
        info( "Log level verbose \n")
        setLogLevel('debug')
    else:
        info( "Log level info \n")
        setLogLevel('info')


if __name__ == '__main__':
    setLogLevel( 'info' )
    opts = OptionParser()
    opts.add_option('--node', '-n',
                    dest="nodes",
                    type='int',
                    default=3)
    opts.add_option('-v', '--verbose',
                    action='callback',
                    callback=verbose,
                    default=None)
    opts.add_option('-i', '--iface',
                    action='append',
                    default=[])
    opts.add_option('--org',
                    action="store_true",
                    dest="org",
                    default=False)
    opts.add_option('--ingress',
                    dest="ingress",
                    type='string',
                    default=None)
    opts.add_option('--egress',
                    dest="egress",
                    type='string',
                    default=None)
    options, args = opts.parse_args()

    if len(args) > 0:
        for i in args:
            options.iface.append(i)
    else:
        options.iface = ["ens3f0", "ens6f0"]

    if options.org:
        run(options.nodes, None, options.org, options.ingress, options.egress)
        exit()
    info("*** Creating chain topology with %d nodes and using %s as physical functions\n" % (options.nodes, options.iface))
    run(options.nodes, options.iface, False, options.ingress, options.egress)
