#!/usr/bin/env python

"""
This example shows how to add an interface (for example a real
hardware interface) to a network after the network is created.
"""

import sys

from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.net import Mininet
from mininet.link import Intf


def run_testbed():
    info('*** Mininet run for testbed! Creating network\n')
    net = Mininet(sriov=True)
    h1 = net.addHost('h1', ip='10.0.0.1')
    h2 = net.addHost('h2', ip='10.0.0.2')
    h3 = net.addHost('h3', ip='10.0.0.3')
    h4 = net.addHost('h4', ip='10.0.0.4')

    #TODO BA automate this via net.nextVirtualFunction()
    _intf0 = "ens6f0v0"
    _intf1 = "ens6f0v1"
    _intf2 = "ens3f0v0"
    _intf3 = "ens3f0v1"
    ens6f0v0 = Intf(_intf0, node=h1)
    ens6f0v1 = Intf(_intf1, node=h2)
    ens3f0v0 = Intf(_intf2, node=h3)
    ens3f0v1 = Intf(_intf3, node=h4)
    return net

def run_local():
    info('*** Mininet run for local! Creating network\n')
    net = Mininet(sriov=True)
    s1 = net.addSwitch('s1')
    h1 = net.addHost('h1', ip='10.0.0.1')
    _intf0 = "enp1s0"
    enp1s0 = Intf(_intf0, node=h1)

    #net.addLink(h1, h1)
    net.addLink(h1, s1)
    return net

if __name__ == '__main__':

    num_args = len(sys.argv)
    argv = sys.argv

    print(argv)

    #setLogLevel('debug')
    setLogLevel('info')
    net = None

    if num_args > 1:
        net = run_testbed()
    else:
        net = run_local()

    net.start()
    CLI(net)
    net.stop()
