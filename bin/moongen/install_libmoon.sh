#!/bin/bash

if [ "$USER" != "root" ];then
    echo "Please run as root!"
    exit
fi

read -p "Install libmoon? (y/N)" INPUT
echo ""

if [ "$INPUT" == "y" ] || [ "$INPUT" == "Y" ]; then
    echo ""
    echo "-> installing required dependencies..."
    echo ""
    apt-get install -y git build-essential cmake linux-headers-`uname -r` lshw libnuma-dev libssl-dev

	  CUR_DIR=$(pwd)

	  cd $HOME

	  echo ""
	  echo "-> installing Libmoon into $HOME"
	  echo ""

    GIT_REPO="https://github.com/WiednerF/libmoon.git"
    GIT_BRANCH="dpdk-20.08"
    GIT_COMMIT="a6525dda82c2d82a82d8e2fa6129f729f81d843c"
    FORWARDER=libmoon

    git clone --branch "$GIT_BRANCH" --recurse-submodules "$GIT_REPO" "$FORWARDER"
    cd $FORWARDER/
    git checkout "$GIT_COMMIT"

    # prevent build.sh from executing bind_interfaces.sh and bind
    BUILD="build.sh"
    sed -i -e 's/\.\/bind\-interfaces\.sh /#.bind-interfaces.sh /g' $BUILD

    echo ""
    echo "-> building libmoon..."
    echo ""

	  ./build.sh
    ./setup-hugetlbfs.sh

    # load igb_uio driver
    modprobe uio
    (lsmod | grep igb_uio > /dev/null) || insmod ./deps/dpdk-kmods/linux/igb_uio/igb_uio.ko

    # fix for missing library
    echo "$HOME/$FORWARDER/build/tbb_cmake_build/tbb_cmake_build_subdir_release" >> /etc/ld.so.conf.d/libmoon.conf
    ldconfig

    ./deps/dpdk/usertools/dpdk-devbind.py --bind=igb_uio ens3f0v0 ens6f0v0
    ./deps/dpdk/usertools/dpdk-devbind.py --bind=igb_uio enp33s0f1v0 ens6f0v0


	  cd $CUR_DIR

	  echo ""
	  echo "do not forget to bind interfaces with $HOME/libmoon/deps/dpdk/usertools/dpdk-devbind.py"
	  echo ""
fi
