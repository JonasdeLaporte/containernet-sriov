#!/bin/bash

# This script installs MoonGen, installs iavf driver and loads the drivers igb_uio, iavf
# sets up hugetables and binds the interfaces defined with $EGRESS and $INGRESS to
# the uio_igb driver

if [ "$USER" != "root" ];then
    echo "Please run as root!"
    exit
fi

if [ $1 == "--help" ]; then
  echo "Usage 'install_moongen.sh <LoadGen>'"
  echo "<LoadGen> might be 'intelexp0' or 'litecoincash'"
  echo ""
  echo "Usage 'install_moongen.sh <egress_iface> <ingress_iface>'"
  exit
fi

# == INTELEXP0 as LoadGen
#EGRESS="ens1f0"
#INGRESS="ens1f1"
# == LITECOINCASH
#EGRESS=eno6
#INGRESS=eno4

if [ $# -eq 1 ]; then
  if [ $1 == "litecoincash" ]; then
    EGRESS=eno6
    INGRESS=eno4
  else
    EGRESS="0000:18:00.0"
    INGRESS="0000:18:00.1"
  fi
else
  EGRESS=$1
  INGRESS=$2
fi

echo "-> Using egress interface $EGRESS and ingress interface $INGRESS"
echo ""

CUR_DIR=$(pwd)

TMP=1

if [ $TMP -eq 1 ]; then # ===========================================================0

echo "-> installing required dependencies..."
echo ""
apt-get install -y build-essential cmake linux-headers-`uname -r` pciutils libnuma-dev

cd $HOME

if [ $1 != "intelexp0" ]; then

echo "-> installing IAVF driver..."
echo ""
git clone https://github.com/dmarion/iavf.git

cd iavf/src
# This version of the iavf driver relies on older kernels.
# The read_barrier_depends macro was removed: https://lkml.org/lkml/2020/6/30/886
# The changes suggested here (check the referenced commits) https://github.com/ntop/PF_RING/issues/758
# propose to use smp_rmb instead.
sed -i 's/read_barrier_depends();/smp_rmb();/g' iavf_txrx.c
make install
modprobe iavf

fi

fi # ===========================================

cd $HOME

echo ""
echo "-> installing MoonGen into $HOME"
echo ""

#LOADGEN='moongen'

git clone https://github.com/emmericp/MoonGen.git
cd MoonGen
git submodule update --init

# prevent build.sh from executing bind_interfaces.sh and bind
sed -i -e 's/\.\/bind\-interfaces\.sh /#.bind-interfaces.sh /g' libmoon/build.sh
./build.sh
echo ""
echo "-> setting up hugetlbfs"
./setup-hugetlbfs.sh

# load igb_uio driver
echo ""
echo "-> loading igb_uio driver"
cd ./libmoon/deps/dpdk/x86_64-native-linuxapp-gcc/kmod/
modprobe uio
insmod igb_uio.ko

echo ""
echo "-> binding $EGRESS and $INGRESS to DPDK driver igb_uio"
# this binds the EGRESS and INGRESS interfaces to the DPDK driver
cd $HOME/MoonGen
./libmoon/deps/dpdk/usertools/dpdk-devbind.py --bind=igb_uio "$EGRESS" "$INGRESS"

cd $CUR_DIR

echo ""
echo "do not forget to bind interfaces with .../MoonGen/libmoon/deps/dpdk/usertools/dpdk-devbind.py"